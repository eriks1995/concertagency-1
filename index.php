﻿<?php
include("header.php");
?>

<div class="content">
    <div class="welcome">
        <p>Welcome!</p>
    </div>
    <table>
        <?php foreach (model_getConcerts() as $row):?>
            <tr>
                <td class='series'>
                    <?= $row["name"]?><br>
                    <?= $row["time"]?><br>
                    <?= $row["address"]?><br>
                    <?= $row["comment"]?><br>
                </td>
                <td>
                    <img src="images/<?= $row["image"]?>" width=430 height=200>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div><!-- content ends-->

<?php
include("footer.php");
?>
