<?php

function message_add($message)
{
    if (empty($_SESSION['messages'])) {
        $_SESSION['messages'] = array();
    }
    $_SESSION['messages'][] = $message;
}

function message_list()
{
    if (empty($_SESSION['messages'])) {
        return array();
    }
    $messages = $_SESSION['messages'];
    $_SESSION['messages'] = array();

    return $messages;
}

function controller_addUser($username, $password1, $password2, $name) {
    if ($password1 === $password2) {
        model_addUser($username, $password1, $name);
        
        header("Location: secure.php");
    }
}